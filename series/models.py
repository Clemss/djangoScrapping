from django.db import models

# Create your models here.

class Serie(models.Model):

    name = models.CharField(max_length=250,unique=True,default="")
    description = models.TextField(blank=True)
    finished = models.BooleanField(default=False)
